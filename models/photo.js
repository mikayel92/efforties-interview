var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var photoSchema = new Schema({
    id :  {type: String, required: true, unique: true},
    title: String,
    tags: [String],
    url : String,
    private : {type: Boolean, default: false},
    user_id : String,
    img: { data: Buffer, contentType: String},
    created_at: Date,
    updated_at: Date
});

photoSchema.pre('save', function(next) {
    // get the current date
    var currentDate = new Date();

    // change the updated_at field to current date
    this.updated_at = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.created_at)
        this.created_at = currentDate;

    next();
});

var Photo = mongoose.model('Photo', photoSchema);

module.exports = Photo;