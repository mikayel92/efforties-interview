// app/routes.js

// load up the user model
var User       = require('../models/user');
var Photo      = require('../models/photo');
var multer     = require('multer');
var fs         = require('fs');
var uniqid     = require('uniqid');
var upload     = multer({ dest: '../uploads' });

module.exports = function(app, passport) {

    // route for home page
    app.get('/', function(req, res) {
        res.render('index.ejs'); // load the index.ejs file
    });

    // route for showing the profile page
    app.get('/profile', isLoggedIn, function(req, res) {
        Photo.find({'user_id': req.user.id}, function (err, photos) {
            if (err)
                throw error;

            var thumbs = [];

            photos.forEach(function (value) {
                thumbs.push({ src : new Buffer(value.img.data).toString('base64')});
            });

            res.render('profile.ejs', {
                user : req.user, // get the user out of session and pass to template
                photos : thumbs
            });
        });
    });

    // route for showing all users
    app.get('/users', isLoggedIn, function(req, res) {
        // get all the users
        User.find({'id' : { $ne: req.user.id }}, function(err, users) { // to not show logged in user
            if (err)
                throw err;

            // render all users
            res.render('users.ejs', {
                users : users
            });
        });
    });

    app.post('/upload-image', isLoggedIn, upload.single('image'), function (req, res, next) {
        var newPhoto = new Photo();
        newPhoto.id = uniqid();
        newPhoto.user_id = req.user.id;
        newPhoto.img.data = fs.readFileSync(req.file.path);
        newPhoto.img.contentType = req.file.mimetype;
        newPhoto.save(function(err) {
            if (err)
                throw err;

            res.redirect('/profile');
        });
    });

    // route for facebook authentication and login
    app.get('/auth/facebook', passport.authenticate('facebook', {
        scope : ['public_profile']
    }));

    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect : '/profile',
            failureRedirect : '/fail'
        }));

    // route for logging out
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    console.log('checking login');
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}